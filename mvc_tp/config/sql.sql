#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: contact
#------------------------------------------------------------

CREATE TABLE contact(
        id_contact  int (11) Auto_increment  NOT NULL ,
        nom         Varchar (100) NOT NULL ,
        prenom      Varchar (100) NOT NULL ,
        tel         Varchar (25) NOT NULL ,
        email       Varchar (100) NOT NULL ,
        message     Text NOT NULL ,
        id_civilite Int NOT NULL ,
        id_objet    Int NOT NULL ,
        PRIMARY KEY (id_contact )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: civilite
#------------------------------------------------------------

CREATE TABLE civilite(
        id_civilite int (11) Auto_increment  NOT NULL ,
        libelle     Varchar (25) NOT NULL ,
        PRIMARY KEY (id_civilite )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: objet
#------------------------------------------------------------

CREATE TABLE objet(
        id_objet int (11) Auto_increment  NOT NULL ,
        libelle  Varchar (100) NOT NULL ,
        PRIMARY KEY (id_objet )
)ENGINE=InnoDB;

ALTER TABLE contact ADD CONSTRAINT FK_contact_id_civilite FOREIGN KEY (id_civilite) REFERENCES civilite(id_civilite);
ALTER TABLE contact ADD CONSTRAINT FK_contact_id_objet FOREIGN KEY (id_objet) REFERENCES objet(id_objet);
