<?php
/**
 * chargement automatique des fichiers de Class
 * @param string $class_name
 */
function __autoload($class_name = ""){
	$modules = array(
			"",
			"modules/contact/",
			"modules/meteo/",
            "modules/sport/"
	);

	$repertoires = array(
			"controllers/",
			"models/",
			"views/",
			"system/",
			"config/"
	);
	foreach ($modules as $module){
		foreach($repertoires as $repertoire){
			$file = "{$module}{$repertoire}{$class_name}.php";
			if(file_exists($file)){
				include_once($file);
			}
		}
	}

}
?>