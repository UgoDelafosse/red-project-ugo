<?php 
class Config{
	
	/**
	 * Propriétés statiques
	 */
	public static $base_href;
	public static $bdd;
	public static $file_contact;
	public static $theme;
	public static $module;
	public static $url_meteo;
	
	/**
	 * Initialisation des propriétés
	 */
	static function init(){
		self::$base_href = "http://".$_SERVER['HTTP_HOST']."/Workspace/red-project-ugo/mvc_tp/";
		self::$bdd = array(
			'host' => "localhost",
			'user' => "root",
			'password' => "",
			'database' => "sport"
		);
		self::$file_contact = "docs/contacts.csv";
		self::$theme = "default";
		self::$module = "page";
		self::$url_meteo = "http://www.prevision-meteo.ch/services/json/";
	}
	/**
	 * setModule
	 * @param string $module
	 */
	static function setModule($module){
		self::$module = $module;
	}
}
Config::init();
?>