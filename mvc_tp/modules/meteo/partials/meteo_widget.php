<?php 
//var_dump($datas['fcst_day_0']['hourly_data']); 
$heure = date("G")."H00";
?>
<table class="table">
	<thead>
		<tr class='success'><td><?= $datas['city_info']['name']?></td></tr>
	</thead>
	<tbody>
		<tr class="success">
			<td><img src="<?=$datas['fcst_day_0']['hourly_data'][$heure]['ICON']?>"/></td>
		</tr>
		<tr class="success">
			<td><?=$datas['fcst_day_0']['hourly_data'][$heure]['TMP2m']."°C"?></td>
		</tr>
	</tbody>
</table>

<style>
	div#widgetmeteo table.table{
		width:80%;
		margin:auto;
	}
	div#widgetmeteo table.table thead tr td {
		font-size:1.5em;
		font-weight:bold;
	}
	div#widgetmeteo table.table tbody tr td {
		border:0px;
		font-size:1.5em;
	}
		
</style>