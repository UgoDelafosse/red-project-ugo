<?php 
class ViewMeteo{
	
	public function afficherMeteo($datas=array()){
		$params = array(
			'content' => "meteo"	
		);
		include "themes/".Config::$theme."/theme.php";
	}
	
	public function afficherErreur($datas=array(),$partial='erreur'){
		$params = array(
				'content' => "{$partial}"
		);
		include "themes/".Config::$theme."/theme.php";
	}
	
	public function afficherWidgetMeteo(){
		$datas = $_SESSION['meteo'];
		$params = array(
				'content' => "widget"
		);
		include "themes/".Config::$theme."/ajax.php";
	}
	
	public function afficherWidgetErreur(){
		
	}
	
}


?>