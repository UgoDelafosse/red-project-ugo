<?php

class ModelSport{

	private $table;
	private $file;
	private $dao;
	private $table_;
	private $table_;

	/**
	 * constructeur
	 */
	public function __construct(){
		$this->table = 'sport';
		$this->dao = new DAO;
		//$this->table_ = '';
		//$this->table_ = '';
	}

	public function getListeSports(){
		$reponse =false;
		$sql ="SELECT `nom` FROM `sport_sport` WHERE `supprimer`=0";
		$dao = new DAO();
		if(!$dao->bddConnexion()){
			$result = $dao->bddQuery($sql);
			while ($myrow = $result->fetch_assoc()){
				$reponse[] = $myrow;
			}
			$dao->bddDeconnexion();
		}
		return $reponse;
	}

	public function getUrl($sport){
		$reponse =false;
		$sql ="SELECT sport_sport.nom,sport_fluxrss.url_flux 
		FROM sport_sport
		LEFT JOIN sport_est_relie_a ON sport_sport.id_sport = sport_est_relie_a.id_sport 
		LEFT JOIN sport_fluxrss ON sport_est_relie_a.id_fluxRSS = sport_fluxrss.id_fluxRSS 
		WHERE nom = '{$sport}'
		AND sport_est_relie_a.supprimer = 0";
		$dao = new DAO();
		if(!$dao->bddConnexion()){
			$result = $dao->bddQuery($sql);
			while ($myrow = $result->fetch_assoc()){
				$reponse[] = $myrow;
			}
			$dao->bddDeconnexion();
		}
		return $reponse;

	}

	public function getListeSources($source){
		$reponse =false;
		$sql ="SELECT `nom_site`, `url_flux` FROM `sport_fluxrss` WHERE `nom_flux` = 'general' AND `supprimer` = 0 ";
		$dao = new DAO();
		if(!$dao->bddConnexion()){
			$result = $dao->bddQuery($sql);
			while ($myrow = $result->fetch_assoc()){
				$reponse[] = $myrow;
			}
			$dao->bddDeconnexion();
		}
		foreach ($reponse as $array){
			if(in_array($source,$array))
			$_SESSION['liste_source'] = array($array['nom_site']=>$array['url_flux']);
		}
		$this->afficherSources($reponse);
	}

	public function afficherSources($listeSource){
		$dropdowns ="";
		foreach ($listeSource as $source){
			$dropdowns.= "<li><a href='accueilSport/source/{$source['nom_site']}'>{$source['nom_site']}</a></li> ";
		}
		$_SESSION['dropdown-source'] = $dropdowns;
	}

	public function getRSS($Urls){
		$RSS =array();
		foreach ($Urls as $Url){
			//	var_dump($Url['Url_flux']);
			array_push($RSS,file_get_contents($Url['url_flux']));
		}
		return ($RSS);
		/* $RSS = file_get_contents($Urls);
		return $RSS; */
	}
	public function enregistrerSport($newSport){

		$ok = true;

		$sql ="INSERT INTO `sport_sport` (`id_sport`, `nom`, `supprimer`) VALUES (null,'{$newSport}', '0')";

		$dao = new DAO;
		if(!$dao->bddConnexion()){
			if(!$dao->bddQuery($sql))
				$ok = false;
			$dao->bddDeconnexion();
		}
		else{
			$ok = false;
		}
		return $ok;
	}
}
?>