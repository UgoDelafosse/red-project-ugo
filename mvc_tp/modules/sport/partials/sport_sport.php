<?php
$url_rss = "http://www.lequipe.fr/rss/actu_rss.xml";
//$url_rss = $_SESSION['source_carousel'];
//http://www.lequipe.fr/rss/actu_rss.xml;
//http://www.sport.fr/RSS/sport.xml
//http://www.sports.fr/fr/cmc/rss.xml
//http://rmcsport.bfmtv.com/rss/info/flux-rss/flux-toutes-les-actualites/

$rss = file_get_contents($url_rss);

//var_dump($rss);
// http://image.noelshack.com/fichiers/2017/22/1496141320-pas-image.png

$objRss = new SimpleXMLElement($rss);
?>

	<form class="navbar-form navbar-left" action="sport" method="post">
	<div class="btn-group" style="float: left">
		<button class="btn btn-default btn-ms dropdown-toggle" type="button"
			data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			Sports <span class="caret"></span>
		</button>
		<ul id="dropdown-sport" class="dropdown-menu">
  <?php echo $_SESSION['dropdown-sport']?>  
  </ul>
	</div>

	<div class="form-group">
			<input type="text" name="sport" class="form-control"
				placeholder="Saisir un critère">
		</div>
		<button type="submit" class="btn btn-primary">Recherche</button>
	</form>

<br><br><br>
	
	<div class="container" style="max-width:500px; width:100%; max-height:350px">
		<div class="btn-group">
		  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		    Source<span class="caret"></span>
		  </button>
		  <ul id="dropdown-source" class="dropdown-menu">
		  	<?php echo $_SESSION['dropdown-source']?>
		  </ul>
		</div>
	  <div id="myCarousel" class="carousel slide" data-ride="carousel">
	    <!-- Indicators -->
	    <ol class="carousel-indicators">
	      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
	      <li data-target="#myCarousel" data-slide-to="1"></li>
	      <li data-target="#myCarousel" data-slide-to="2"></li>
	    </ol>
	
	    <!-- Wrapper for slides -->
	    <div class="carousel-inner">
	
	      <div class="item active" >
				<object data="<?= $objRss->channel->item[0]->enclosure['url'] ?>" style="width:100%; type="image/png">
	        	<img src="http://image.noelshack.com/fichiers/2017/22/1496141320-pas-image.png" style="width:100%;">
	        	</object>
	        	<div class="carousel-caption" href="<?= $objRss->channel->item[0]->link ?> ">
	          <a href="<?= $objRss->channel->item[0]->link ?>" style="color:#F0FFFF" target="_blank" ><p><?= $objRss->channel->item[0]->title ?></p></a>
	        </div>
	      </div>
	
	      <div class="item">
	      	    <object data="<?= $objRss->channel->item[1]->enclosure['url'] ?>" style="width:100%; type="image/png">
	        	<img src="http://image.noelshack.com/fichiers/2017/22/1496141320-pas-image.png" style="width:100%;">
	        	</object>
	        <div class="carousel-caption">
	          <a href="<?= $objRss->channel->item[1]->link ?>" style="color:#F0FFFF" target="_blank" ><p><?= $objRss->channel->item[1]->title ?></p></a>
	        </div>
	      </div>
	    
	      <div class="item">
	        <object data="<?= $objRss->channel->item[2]->enclosure['url'] ?>" style="width:100%; type="image/png">
	        <img src="http://image.noelshack.com/fichiers/2017/22/1496141320-pas-image.png" style="width:100%;">
	        </object>
	        <div class="carousel-caption">
	          <a href="<?= $objRss->channel->item[2]->link ?>" style="color:#F0FFFF" target="_blank" ><p><?= $objRss->channel->item[2]->title ?></p></a>
	        </div>
	      </div>
	  
	    </div>
	
	    <!-- Left and right controls -->
	    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
	      <span class="glyphicon glyphicon-chevron-left"></span>
	      <span class="sr-only">Previous</span>
	    </a>
	    <a class="right carousel-control" href="#myCarousel" data-slide="next">
	      <span class="glyphicon glyphicon-chevron-right"></span>
	      <span class="sr-only">Next</span>
	    </a>
	  </div>
	</div>

