<?php
class ViewSport{

	public function afficherSports($listeSport,$contenu="carousel"){
		$params['content']='acceuil';
		$content['content']= $contenu;
		$dropdown ="";
		foreach ($listeSport as $sport){
			$dropdown.= "<li><a href='afficherFluxRss/getFluxRss/{$sport['nom']}'>{$sport['nom']}</a></li> ";
		}
		$_SESSION['dropdown-sport'] = $dropdown;
		include "themes/".Config::$theme."/theme.php";
	}

	public function afficherInfos($rss){
		$params['content']='acceuil';
		$content['content']='afficher';
		include "themes/".Config::$theme."/theme.php";

	}
	public function afficherErreurDeSaisie($datas=array(),$partial='nonsaisi'){
		$params['content']='acceuil';
		$content = array(
			'content' => "{$partial}"
		);
		include "themes/".Config::$theme."/theme.php";
	}

	public function afficherAccueilAdmin()
	{
		$params = array(
			'content' => "administration"
		);
		include "themes/".Config::$theme."/theme.php";
	}
	
	public function afficherFormSport(){
		$params = array(
			'content' => "ajouterSport"
		);
		include "themes/".Config::$theme."/theme.php";
	}

	public function afficherEnregistrementErreur($datas=array(), $partials='erreur_enregistrement'){
		$params = array(
			'content' => "{$partials}"
		);
		include "themes/".Config::$theme."/theme.php";
	}

	public function afficherErreurSportDejaExistant($partials='erreur_sport_deja_existant'){
		$params = array(
			'content' => "{$partials}"
		);
		include "themes/".Config::$theme."/theme.php";
	}
	
}
	


?>