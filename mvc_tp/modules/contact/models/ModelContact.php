<?php

class ModelContact{

	private $table;
	private $file;
	private $table_civilite;
	private $table_objet;

	/**
	 * constructeur
	 */
	public function __construct(){
		$this->table = 'contact';
		$this->file = Config::$file_contact;
		$this->table_civilite = 'civilite';
		$this->table_objet = 'objet';
	}

	/**
	 * inserer les données de formulaire dans un fichier csv et dans la base de données
	 * @param array $datas
	 * @return boolean
	 */
	public function inserer($datas=array()){
		
		$ok = true;
		// Sauvegarde des donn�es dans un fichier .csv
		$this->saveCSV($datas);
		$message = addslashes($datas['message']);
		// Creer requete SQL
		$sql ="INSERT INTO {$this->table} VALUES (
		NULL,
		'{$datas['nom']}',
		'{$datas['prenom']}',
		'{$datas['tel']}',
		'{$datas['email']}',
		'{$message}',
		'{$datas['id_civilite']}',
		'{$datas['id_objet']}'
		);";
		// Enregistrer en base de donn�es
		$dao = new DAO;
		if(!$dao->bddConnexion()){
			if(!$dao->bddQuery($sql))
				$ok = false;
			$dao->bddDeconnexion();
		}
		else{
			$ok = false;
		}
		return $ok;
	}
	
	public function getCivilites(){
		$reponse = false;
		$sql = "SELECT * FROM {$this->table_civilite}";
		$dao = new DAO;
		if(!$dao->bddConnexion()){
			$result = $dao->bddQuery($sql);
			while ($myrow = $result->fetch_assoc()){
				$reponse[] = $myrow;
			}
			$dao->bddDeconnexion();
		}
		return $reponse;
	}

	public function getObjets(){
		$reponse = false;
		$sql = "SELECT * FROM {$this->table_objet}";
		$dao = new DAO;
		if(!$dao->bddConnexion()){
			$result = $dao->bddQuery($sql);
			while ($myrow = $result->fetch_assoc()){
				$reponse[] = $myrow;
			}
			$dao->bddDeconnexion();
		}
		return $reponse;
	}
	
	public function readNbStats(){
		$reponse = false;
		// preparer requete SQL
		$sql = "SELECT
					SUM(CASE WHEN co.id_civilite = '1' THEN 1 ELSE 0 END) AS 'Nb Femmes',
					SUM(CASE WHEN co.id_civilite = '2' THEN 1 ELSE 0 END) AS 'Nb Hommes'
					FROM contact AS co
					";
		$dao = new DAO();
		if(!$dao->bddConnexion()){
			$result = $dao->bddQuery($sql);
			while ($myrow = $result->fetch_assoc()){
				$reponse[] = $myrow;
			}
			$dao->bddDeconnexion();
		}
		return $reponse;
	}
	
	public function readDetailsByObjet(){
		$reponse = false;
		// preparer requete SQL
		$sql = "SELECT
					co.id_contact, co.nom, co.prenom, co.tel, ci.libelle as civilite, o.id_objet, o.libelle as objet
					FROM contact AS co
					JOIN civilite AS ci
					ON co.id_civilite = ci.id_civilite
					JOIN objet AS o
					ON co.id_objet = o.id_objet
					ORDER BY co.id_contact DESC
					";
		$dao = new DAO();
		if(!$dao->bddConnexion()){
			$result = $dao->bddQuery($sql);
			while ($myrow = $result->fetch_assoc()){
				$reponse[] = $myrow;
			}
			$dao->bddDeconnexion();
		}
		return $reponse;
	}
	
	/**
	 * sauvegarder dans un fichier csv
	 * @param array $datas
	 * @return boolean
	 */
	private function saveCSV($datas=array()){
		$ok = false;
		$f_exist = file_exists($this->file);
		$fp = fopen($this->file, 'a');
		if($fp){
			if(!$f_exist)
			{
				$ok = fputcsv($fp, array_keys($datas), ";", '"');
			}
			$ok = fputcsv($fp, $datas, ";", '"');
			fclose($fp);
		}
		return $ok;
	}
}
?>