<?php 
	//var_dump($datas['nbStats']);
	//var_dump($datas['objStats']); 
?>
<h1>Statistiques</h1>
<h3>Nombres de contacts</h3>
<br/>
<table class='table' style='width:200px;margin:auto;'>
	<tbody>
	<?php foreach($datas['nbStats'][0] as $titre => $nombre) : ?>
		<tr>
			<td><?= $titre ?> : </td>
			<td><?= $nombre ?></td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>

<?php 
$objets = array();
	foreach($datas['objStats'] as $v){
		$objets[$v['id_objet']][] = $v;
	}
?>

<?php foreach($objets as $objet) : ?>

<h3><?= $objet[0]['objet'] ?></h3>
<table class='table'>
	<thead>
		<tr>
			<th>#</th><th>civilité</th><th>nom</th><th>prénom</th><th>Tel</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($objet as $o) : ?>
		<tr>
			<td><?= $o['id_contact'] ?></td>
			<td><?= $o['civilite'] ?></td>
			<td><?= $o['nom'] ?></td>
			<td><?= $o['prenom'] ?></td>
			<td><?= $o['tel'] ?></td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>
<?php endforeach;?>

<br/>
