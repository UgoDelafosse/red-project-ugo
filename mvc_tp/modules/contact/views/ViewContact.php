<?php 
class ViewContact{
	
	public function afficherForm($datas=array()){
		$params = array(
			'content' => "contact"	
		);
		include "themes/".Config::$theme."/theme.php";
	}
	
	public function afficherErreurDeSaisie($datas=array(),$partial='erreurBdd'){
		$params = array(
				'content' => "{$partial}"
		);
		include "themes/".Config::$theme."/theme.php";
	}
	
	public function afficherResultat(){
		$params = array(
				'content' => "resultatBdd"
		);
		include "themes/".Config::$theme."/theme.php";
	}
	
	public function afficherStats($datas = array()){
		$params = array(
				'content' => "stats"
		);
		include "themes/".Config::$theme."/theme.php";
	}
}


?>