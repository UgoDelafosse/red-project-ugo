
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      	<a class="navbar-brand" href="" style="padding:0;">
					<img alt="Logo AFPA" src="themes/<?= Config::$theme?>/images/logo.jpg" style="height:50px;">
				</a>
		    </div>
		
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="dropdown">
		          <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Contact<span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="contact">Form. Contact</a></li>
		            <li><a href="stats">Statistiques</a></li>
		          </ul>
		        </li>
		      </ul>
            <ul class="nav navbar-nav" id="menu-deroulant">
                <li class="dropdown">
                    <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sports<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="accueilSport">Accueil</a>
                        <li><a href="adminsports">Administration</a></li>
                    </ul>
                </li>
            </ul>
		      <form class="navbar-form navbar-left" action="meteo" method="post">
		        <div class="form-group">
		          <input type="text" name="ville" class="form-control" placeholder="Saisir une ville">
		        </div>
		        <button type="submit" class="btn btn-primary">Météo</button>
		      </form>
		      <ul class="nav navbar-nav navbar-right">
		        <li><a href="#">Link</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="#">Action</a></li>
		            <li><a href="#">Another action</a></li>
		            <li><a href="#">Something else here</a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="#">Separated link</a></li>
		          </ul>
		        </li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>