<?php
session_start();
include_once('config/autoload.php');

$c = (isset($_GET['c']) && $_GET['c'] != '') ? $_GET['c'] : "CtrlContact";
$m = isset($_GET['m']) ? $_GET['m'] : "getForm";
$a = isset($_GET['a']) ? $_GET['a'] : "";

/**
 * Route
 */
$route = array();
$route['contact'] = array(
		'c' => "CtrlContact",
		'm' => "getForm",
		'a' => "",
		'module' => "contact"
);
$route['savecontact'] = array(
		'c' => "CtrlContact",
		'm' => "enregistrerForm",
		'a' => "",
		'module' => "contact"
);
$route['stats'] = array(
		'c' => "CtrlContact",
		'm' => "getStats",
		'a' => "",
		'module' => "contact"
);
$route['meteo'] = array(
		'c' => "CtrlMeteo",
		'm' => "getMeteo",
		//'a' => "toulouse",
		'a' => "",
		'module' => "meteo"
);
$route['widgetmeteo'] = array(
		'c' => "CtrlMeteo",
		'm' => "getWidgetMeteo",
		'a' => "",
		'module' => "meteo"
);
$route['accueilSport'] = array(
    'c' => "CtrlSport",
    'm' => "getModuleSport",
    'a' => "",
    'module' => "sport"
);
$route['afficherFluxRss'] = array(
    'c' => "CtrlSport",
    'm' => "getInfos",
    'a' => "",
    'module' => "sport"
);
$route['sport'] = array(
    'c' => "CtrlSport",
    'm' => "getSport",
    'a' => "",
    'module' => "sport"
);
$route['afficherSource'] = array(
    'c' => "CtrlSport",
    'm' => "getSources",
    'a' => "",
    'module' => "sport"
);
$route['adminsports'] = array(
    'c' => "CtrlSport",
    'm' => "getAccueilAdministration",
    'a' => "",
    'module' => "sport"
);
$route['ajouterSport'] = array(
    'c' => "CtrlSport",
    'm' => "getFormSport",
    'a' => "",
    'module' => "sport"
);
$route['newSport'] = array(
    'c' => "CtrlSport",
    'm' => "addSport",
    'a' => "",
    'module' => "sport"
);

if(isset($route[$c])){
	(isset($route[$c]['module'])) ? Config::setModule($route[$c]['module']) : null;

	$ctrl = new $route[$c]['c']; // $ctrl = new CtrlMeteo;
	//$ctrl->$route[$c]['m']($route[$c]['a']); // $ctrl->getMeteo('toulouse');
    $ctrl->$route[$c]['m']($a); // $ctrl->getMeteo();
}
else{
	/**
	 * TODO gerer les pages du module page
	 * Si pas de page afficher une page d'erreur
	 */
	echo "ERREUR 404";
}

?>